# Build your own Docker
_Ou comment créer son système de conteneur pour mieux en comprendre les mécanismes_

## Introduction

**Un environnement d'exècution isolé**
Qu'est-ce qu'un container ? Un environnement d'éxecution isolé. Si vous savez déjà répondre ça, c'est une bonne base.
Quels sont les mécanismes qui permettent cette isolation ? Quelle est la magie qui permet de faire tourner un busybox sur un Ubuntu, tout en étant léger ?

C'est ce que nous allons voir via cette démonstration.

**Pourquoi isoler des process ?**

Isoler des process permet de mieux les controler. Sans isolation, on peut imaginer qu'un process *foo* supprime des fichiers utilisés par le process *bar*, ou qu'un process *baz* supprime une interface réseau utilisée par un process *qux*. Isoler ces process permet de supprimer ces risques et de faire tourner *foo* sans même qu'il sache que *bar*, *baz* et *qux* existent.

Il est difficile d'interagir avec ce que l'on ne voit pas. Et c'est exactement sur ça que repose la containerisation: un moyen simple de cacher le reste de la machine hôte à un process.

Cette isolation se base sur un mécanisme du noyau linux: les namespaces. Ce sont eux qui permettent de lui faire croire qu'il est le seul à fonctionner sur la machine hôte.

A noter que ces namespaces ne permettent pas de restreindre l'accès aux ressources physiques (RAM, CPU, espace disque) de la machine aux process. Cette partie (qui n'est pas nécessaire pour l'isolation), se base sur les *cgroups* (control groups).


### Isolation

Qu'est ce que l'on peut isoler ?

Il existe 7 catégories d'isolation, qui permettent de simuler que le process tourne seul sur l'hôte:

1. Mount, isole le point de montage du système de fichier
2. UTS, Unix Timesharing System, permet d'isoler l'hostname et le domain name [ajouter détail sur le domainname]
3. IPC, Inter process communication, isole la communication entre process.  (Communication entre process par  mécanismes de mémoire partagée ou par message) [ajouter pour la conférence les références vers les fonctions C associées]
4. PID, Isoler l'espace PID des processus
5. Réseau, isoler les interfaces réseau
6. Isoler l'espace des UID, GID
7. CGroup, isoler les ressources

Et vous savez quoi ? Même pas besoin de docker pour expérimenter tout ça ! `man unshare`.

**Les 3 syscalls des namespaces**

1. `clone` pour créer de nouveaux process, avec ses multiples flags d'isolation (exemple: `CLONE_NEWUTS` ou `CLONE_NEWPID`).
2. `setns` pour rejoindree des namespaces déjà existants
3. `unshare` pour créer un namespace pour un process

**Démo rapide**

```
# Assuming running as root
hostname
unshare -u /bin/bash # Create a shell in new UTS namespace
    hostname hello-demo
    hostname
exit
hostname
```

**Lecture en +**: http://man7.org/linux/man-pages/man7/namespaces.7.html


## Let's Go

`unshare` est correct pour du script rapide, mais n'est pas optimale lorsque les besoins sont plus complexes. Dans ce cas, il faut quelque chose de plus robuste.

C'est parti pour Golang. Pourquoi golang ? Parce que c'est un langage que nous utilisons beaucoup pour construire le cloud Continental, et que dans toutes la collection de langage que je plus ou moins maitrise (PHP, Js <3), je ne me sens pas agréable à faire du bas niveau avec.

Nous allons donc réaliser un système de containerisation, en jouant avec les 7 namespaces présentées un peu plus haut. Pour commencer, un simple bash isolé du reste de la machine, pour terminer sur un vrai système de container, ou un process tourne en mode root dans un espace d'exécution isolé.

Quels sont les fonctions Go correspondant aux 3 appels systèmes énoncés plus haut ?

 * `clone` fait parti de `exec.Run()`, auquel on peut passer des attributs pour chaque flag de clone.
 * [TODO setns]
 * [TODO unshare]

**DEMO STEP 1: A basic, no izolation process runner**

**DEMO STEP 2: A process in a different UTS**

**Demo2 explanation:** 

`/proc/self/ns/uts`: Le type de namespace et le nombre d'inodes de celui-ci. (inode: structure contenant des infos sur un fichier ou un répertoire).
2 nombre d'inodes différents implique que les 2 process sont dans 2 UTS différents.

**DEMO STEP 3: Some fun with  clone flags**

[Faire une fois sans le `CLONE_NEWUSER` (qui force à être root), et une fois avec.]

[`ps -ae` montre encore les process, car `ps` lit le dossier `/proc`]

Super, on a un conteneur ? Pas vraiment. Le niveau d'isolation est complètement virtuel à ce stade.

 1. `CLONE_NEWNS` nouveau point de montage, mais pour le moment, nous partageons encore `/`.
 2. `CLONE_NEWPID`, mais il y a toujours le dossier `/proc` qui contient les fichier .pid des autres process. (cf la remarque sur ps plus haut).
 3. `CLONE_NEWNET`, mais aucune interface n'est définie dans le namespace
 4. `CLONE_NEWUSER`, mais aucun mapping UID/GID pour le moment donc notre utilisateur est personne et ne peut pas interagir avec le système de fichier (permission denied). [cf résultat des commandes `id` & `whoami`].

 Remarque sur `CLONE_NEWUSER`: Plus besoin de lancer en root le programme appellant, par contre la perte d'identité est génante: nous ne sommes pas reconnus comme root dans notre propre namespace.


## UID & GID mapping

L'UID (User ID) et le GID (Group ID) sont les 2 numéros associés à un utilisateur qui permettent la gestion des droits d'accès sur les fichiers. Perdre ce numéro, nous banni de toute interaction avec le système de fichier.
Ajouter le flag `CLONE_NEWUSER`, n'est pas suffisant, il faut aussi fournir le mapping UID et GID.

En quelques mots:

 * L'isolation des utilisateurs donne une isolation des UID et GID
 * Il peut y avoir plusieurs namespaces utilisateurs en même temps sur une machine.
 * Chaque process linux fonctionne dans un de ces namespaces utilisateur
 * Les namespaces utilisateurs (esnuite User namespaces) permettent que l'UID d'un User namespace A d'être différent de l'UID du même process dans le User namespace B.
 * Le mapping UID/GID est un mécanisme pour faire correspondre les UID/GID entre 2 User namespaces différents.

 Exemple: mapper que l'UID et GID 0 dans le User namespace B correspondent à l'UID et GID de l'appellant (par défaut 1000) dans le namespace A.
 Et donc on peut avoir un process root dans son namespace ne pas avoir les privilèges root dans le User namespace du process père.

 **DEMO STEP4: UID & GID Mapping**

Qu'avons nous fait ? Le code est plutôt auto-descriptif: Nous avons mappé l'UID 0 dans le container à celui de l'appellant et pareil pour le GID.

Ceci nous donne l'interaction avec le système de fichier avec les mêmes permissions que l'appellant (tout en restant root dans notre naemspace, cf `hostname toto`). Exemple trivial: en sortant du shell containerisé, les fichiers nous apparaissent comme appartenant à l'appellant.

## Surcharge de l'environnement d'appel

Ok, donc en résumé, actuellement nous avons un process lancé en tant qu'utilisateur normal, avec les privilèges administrateur dans son namespace et la possibilité d'interagir avec les fichiers (dans notre exemple, les fichiers de l'appellant, mais nous pourrions faire le mapping autrement).

Continuons. Si nous prenons un shell containerisé, nous nous rendons compte que certains paramètres comme le hostname [ou le pid, à vérifier] restent ceux utilisés sur la machine hôte. Ceci n'est pas très isolé.

Il nous faudrait un moyen de dire avant la création du namespace que certaines valeurs doivent être surchargées dans le namespace. Or avec Go, ce n'est pas possible le namespace est créé lors du de `cmd.Run()` et on se retrouve ensuite directement dedans. Pas de callback ou autre mécanisme pour surcharger les valeurs. Pour contourner cette contrainte (qui vient de Go), nous allons devoir tricher.

Le hack consiste à créer créer le namespace et se relancer soi même avec les bons paramètres au lieu de lancer directement la commande souhaitée. Comme nous controlons le second process, nous pouvons manipuler les données que l'on souhaite cacher à la commande cible. Ce méchanisme de relance est possible grâce à `/proc/self/exe` qui lance une copie du process appellant.

Il y a 2 façon d'arriver à ce résultat: 

 * La plus simple: faire un binaire avec 2 commandes, une pour le père `go run main.go run args` et une pour le fils `go run main.go child args`. Par contre cela fait une interface de binaire pas terrible.

 * soit en utilisant un package créé par Docker `rexeec` qui permet de n'exposer qu'une commande d'entrée et va émuler l'existence d'un callback après le fork.

 Pas envie d'installer des dépendances et envie de garder le code le plus simple possible, donc pour cette démo, partons sur une approche à 2 commandes.

(Attention, comme nous relancons le process, il faut absolument défnir $PATH pour que `/proc/self/exe` sache ou trouver le binaire.)

**DEMO STEP5: Reexec & surchareg de l'hostname**

Suite à ces démo, 2 choses: nous sommes bien dans un nouvel espace de PID et notre commande elle la première dans cette espace.
Nous avons pu surcharger une valeur de l'environnement d'appel: l'hostname.


 ## Pivot root

 Ca commence à ressembler à un container. Pour aller plus loin, il faut maintenant s'occuper du système de fichier.

**DEMO STEP6: Pivot root sans proc**

Nous tournon dans un contexte centos, nous avons yum qui tourne (mais ne fonctionne pas).

Mais toutes les valeur dans `/proc` sont accessibles, dont `/proc/mount` avec tous les points de montages de l'hôte.

**DEMO STEP6 BIS: Pivot root + mount d'un /proc perso**

Ok, `/proc` isolé. Si on regarde dans un container, il y a un bon nombre de point de mointage, pour /dev... nous allons rester basique, mais vous avez l'idée.

 ## Ressources

 [Utilisation des CGroups pour limiter l'accès aux ressources]


## Réseau

C'est dur, mais ça va marcher.

**DEMO STEP8: curl google.com**