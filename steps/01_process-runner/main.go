// 01 a simple command runner
package main

import (
	"fmt"
	"os"
	"os/exec"
)

// docker run <container_name> cmd args
// go run main.go run cmd args
func main() {
	switch os.Args[1] {
	case "run":
		run()
	default:
		panic("Unknow command")
	}
}

func run() {
	fmt.Printf("Running %v\n", os.Args[2:])

	cmd := exec.Command(os.Args[2], os.Args[3:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = []string{"CCC=hello"}

	if err := cmd.Run(); err != nil {
		panic(err)
	}
}

/**
 * Demo
 * 1. run a echo command
 * 2. take a shell
 * 3. ps # go is running
 */
