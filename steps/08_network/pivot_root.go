package main

import (
	"fmt"
	"os"
	"path/filepath"
	"syscall"
)

func pivotRoot(newroot string) error {
	putold := filepath.Join(newroot, "/.pivot_root")

	// bind mount newroot to itself - this is a slight hack
	// needed to work around a pivot_root requirement
	if err := syscall.Mount(
		newroot,
		newroot,
		"",
		syscall.MS_BIND|syscall.MS_REC,
		"",
	); err != nil {
		return fmt.Errorf("0 %v", err)
	}

	// create putold directory
	if err := os.MkdirAll(putold, 0700); err != nil {
		return fmt.Errorf("1 %v", err)
	}

	// call pivot_root
	if err := syscall.PivotRoot(newroot, putold); err != nil {
		return fmt.Errorf("2 %v", err)
	}

	// ensure current working directory is set to new root
	if err := os.Chdir("/"); err != nil {
		return fmt.Errorf("3 %v", err)
	}

	// umount putold, which now lives at /.pivot_root
	putold = "/.pivot_root"
	if err := syscall.Unmount(
		putold,
		syscall.MNT_DETACH,
	); err != nil {
		return fmt.Errorf("4 %v", err)
	}

	// remove putold
	if err := os.RemoveAll(putold); err != nil {
		return fmt.Errorf("4 %v", err)
	}

	return nil
}

func exitIfRootfsNotFound(rootfsPath string) {
	if _, err := os.Stat(rootfsPath); os.IsNotExist(err) {
		usefulErrorMsg := fmt.Sprintf(`
"%s" does not exist.
Please create this directory and unpack a suitable root filesystem inside it.`, rootfsPath)

		fmt.Println(usefulErrorMsg)
		os.Exit(1)
	}
}
