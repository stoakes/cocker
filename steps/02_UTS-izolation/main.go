// 02 first namespace notion
package main

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"
)

func main() {
	switch os.Args[1] {
	case "run":
		run()
	default:
		panic("Unknow command")
	}
}

func run() {
	fmt.Printf("Running %v\n", os.Args[2:])

	cmd := exec.Command(os.Args[2], os.Args[3:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = []string{"PS1=cocker #"}

	cmd.SysProcAttr = &syscall.SysProcAttr{
		Cloneflags: syscall.CLONE_NEWUTS,
	}

	if err := cmd.Run(); err != nil {
		panic(err)
	}
}

/**
 * Demo:
 * 1. Get root
 * 2. Take a shell
 * 3. readlink /proc/self/ns/uts
 * 4. hostname toto
 * 5. hostname
 * 5bis. ps
 * 6. exit
 * 7. hostname
 */
